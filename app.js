var createError = require('http-errors');
require('dotenv').config({ path: './.env'})
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require('body-parser');
var cors = require('cors')
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')))

app.use(cors ({
  origin: [process.env.FRONT_END_URL, process.env.LOCAL_FRONT_END_URL],
  methods: ['GET', 'POST', 'PUT', 'DELETE'],
  credentials: true
}))
// parse application/json
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/location', require('./routes/location'))
app.use('/book', require('./routes/book'))
app.use('/collection', require('./routes/collection'))
app.use('/user', require('./routes/user'))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
