const express = require('express')
const router = new express.Router()
const controller = require('../controllers/userController')

router.route('/login')
      .post(controller.login)
router.route('/logout')
      .post(controller.logout)
router.route('/token/new')
      .post(controller.getNewToken)
router.route('/')
      .get(controller.getUserProfile)
router.route('/stat')
      .get(controller.getUserStat)

module.exports = router;