const express = require('express');
const router = new express.Router();
const controller = require('../controllers/bookController');

function checkMultipart(req, res, next) {
      const contentType = req.headers["content-type"];
      // Make sure it's multipart/form
      if (!contentType || !contentType.includes("multipart/form-data")) {
          // Stop middleware chain and send a status
          return res.sendStatus(500);
      }
      next();
}

router.route('/')
      .get(controller.getBooks)
      .post(checkMultipart, controller.create)
router.route('/explore')
      .put(controller.exploreBook)
router.route('/:id')
      .get(controller.getBookById)
      .delete(controller.deleteBook)
      .put(controller.updateBook)
router.route('/download')
      .post(controller.download)
router.route('/:id/addLocation')
      .put(controller.addLocation)

module.exports = router;