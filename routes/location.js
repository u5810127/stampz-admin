const express = require('express');
const router = new express.Router();
const controller = require('../controllers/locationController');

var multer = require('multer');
var storage = multer.diskStorage({
      destination: (req, file, cb) => {
      cb(null, 'public/images/locations')
      },
      filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now())
      }
});
var upload = multer({storage: storage});

function checkMultipart(req, res, next) {
      const contentType = req.headers["content-type"];
      // Make sure it's multipart/form
      if (!contentType || !contentType.includes("multipart/form-data")) {
          // Stop middleware chain and send a status
          return res.sendStatus(500);
      }
      next();
}

router.route('/')
      .get(controller.getLocations)
      .post(checkMultipart, controller.create)
router.route('/:id')
      .get(controller.getLocationById)
      .delete(controller.deleteLocation)
      .put(controller.updateLocation)

module.exports = router;