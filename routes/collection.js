const express = require('express');
const router = new express.Router();
const controller = require('../controllers/collectionController');

router.route('/')
      .get(controller.getBooks);
router.route('/:id')
      .get(controller.getBookById);
router.route('/stamp/:id')
      .post(controller.stamp);
router.route('/:id/complete')
      .put(controller.complete)

module.exports = router;