const httpStatus = require('http-status');
const axios = require('axios');
const controller = require('./bookController');

const getBooks = async (req, res) => 
{
    try
    {
        let config = 
        {
            headers: {  Authorization: req.get('Authorization') }
        }
        axios.get(`${controller.journeyHost}/trip`, config)
        .then(function (response) 
        {
            if (response.status == 200) {
                var tripArr = [];
                response.data.trips.forEach(trip => {
                    var book = {
                        id: trip.id,
                        name: trip.name,
                        description: trip.description,
                        imagePath: trip.imagePath,
                        stampCount: trip.checkIns.length,
                        bookId: trip.plan.id,
                        status: trip.status.name,
                        stamped: trip.checkIns
                    }

                    tripArr.push(book)
                });
                return res.status(response.status).json({ message: response.data.message, collections: tripArr })
            } else {
                return response.data.message
            }  
        })
        .catch(function (error) 
        {
            return res.status(error.response.status).json({ message: error.response.data.message });
        });
    } catch(err) {
        return res.status(err.response.status).json({ message: err.message });
    }
};

const getBookById = async (req, res) => 
{
    try
    {
        var id = req.params.id;
        let config = 
        {
            headers: {  Authorization: req.get('Authorization') }
        }
        axios.get(`${controller.journeyHost}/trip/${id}`, config)
        .then(function (response) 
        {
            if (response.status == 200) {
                var planId = response.data.trip.planId;
                var checkInLocationIds = [];
                response.data.trip.checkIns.forEach(item => {
                    checkInLocationIds.push(item.locationId);
                });
                axios.get(`${controller.journeyHost}/plan/${planId}`, config)
                .then(async function (planResponse) 
                {
                    if (planResponse.status == 200) {
                        var locationIds = [];
                        planResponse.data.plan.planItems.forEach(item => {
                            let location = {
                                id: item.location.id,
                                order: item.order
                            }
                            locationIds.push(location)
                        });
                        let locations = []
                        let previewImages = []
                        for (var i = 0; i < locationIds.length; i++)
                        {
                            let getLocation = await axios.get(`${controller.journeyHost}/location/${locationIds[i].id}`, config);
                            let locationOrder = getLocation.data.location
                            locationOrder.order = locationIds[i].order
                            locations.push(locationOrder)
                            if (locationOrder.images && locationOrder.images.length && previewImages.length != 6) {
                                previewImages.push(locationOrder.images[0].path)
                            }
                        }
                        for (var i = 0; i < locations.length; i++)
                        {
                            var isCheckIn = false;
                            if (checkInLocationIds.indexOf(locations[i].id) > -1)
                            {
                                isCheckIn = true;
                            }
                            locations[i].isCheckIn = isCheckIn
                        }
                        var book = {
                            id: planResponse.data.plan.id,
                            name: planResponse.data.plan.name,
                            description: planResponse.data.plan.description,
                            categories: planResponse.data.plan.categories,
                            imagePath: planResponse.data.plan.imagePath,
                            previewImages: previewImages,
                            locations: locations
                        }
                        return res.status(httpStatus.OK).json({ message: planResponse.data.message, book: book });
                    }
                    else
                    {
                        return res.status(planResponse.status).json({ message: planResponse.data.message });
                    }
                })
                .catch(function (error) 
                {
                    return res.status(error.response.status).json({ message: error.response.data.message });
                });
                
            } else {
                return response.data.message
            }  
        })
        .catch(function (error) 
        {
            return res.status(error.response.status).json({ message: error.response.data.message });
        });
    } catch(err) {
        return res.status(err.response.status).json({ message: err.message });
    }
};

async function getLocationByIds (locationIds, config) 
{
    try
    {   
        var promises = [];
        for (var i = 0; i < locationIds.length; i++)
        {
            console.log(locationIds[i]);
            var location = axios.get(`${controller.journeyHost}/location/${locationIds[i]}`, config);
            promises.push(location);
        }
        var locations = [];
        await axios.all(promises).then(function(results) {
            results.forEach(function(response) {
                locations.push(response.data.location);
            })
        });
        return locations;
    } catch(err) {
        return err.message;
    }
};

const stamp = async (req, res) => 
{
    try
    {
        var tripId = req.params.id;
        let config = 
        {
            headers: {  Authorization: req.get('Authorization') }
        }
        var data = {
            'location': req.body.location,
            'latitude': req.body.latitude,
            'longitude': req.body.longitude
        }
        axios.post(`${controller.journeyHost}/trip/${tripId}/checkin`, data, config)
        .then(function (response) 
        {
            if (response.status == 200) {
                return res.status(response.status).json({ message: response.data.message });
            } else {
                return response.data.message
            }  
        })
        .catch(function (error) 
        {
            res.status(error.response.status).json({ message: error.response.data.message });
        });
    } catch(err) {
        return res.status(err.response.status).json({ message: err.message });
    }
}

const complete = async (req, res) => 
{
    try
    {
        let config = 
        {
            headers: { Authorization: req.get('Authorization') }
        }
        axios.put(`${controller.journeyHost}/trip/${req.params.id}/end`, config)
        .then(function (response) 
        {
            if (response.status == 200) {
                return res.status(httpStatus.OK).json({ message: "Successfully completed book" })
            }
            return res.status(response.status).json({ message: response.data.message })
        })
        .catch(function (error) 
        {
            return res.status(error.response.status).json({ message: error.response.data.message });
        })

    } catch(err) {
        return res.status(err.response.status).json({ message: err.message })
    }
}

module.exports = 
{
    getBooks,
    getBookById,
    stamp,
    complete
}