const httpStatus = require('http-status');
const axios = require('axios');
const controller = require('./bookController');
let FormData = require('form-data');
let fs = require('fs');
let multer = require('multer');
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/images/locations')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + '_' + file.originalname)
    }
});
let upload = multer({
    storage: storage
}).array('images', 12);

const getLocations = async (req, res) => 
{
    try
    {
        let config = 
        {
            headers: {  Authorization: req.get('Authorization') },
            params: { pageSize: 70 }
        }
        axios.get(`${controller.journeyHost}/location`, config)
        .then(function (response) 
        {
            if (response.status == 200) {
                var locations = [];
                response.data.locations.forEach(item => {
                    var location = {
                        id: item.id,
                        name: item.name,
                        description: item.description,
                        shortDescription: item.shortDescription,
                        latitude: item.latitude,
                        longitude: item.longitude,
                        imagePath: item.imagePath,
                        tags: item.tags,
                        r1: item.r1,
                        r2: item.r2
                    }
                    locations.push(location);
                });
                return res.status(httpStatus.OK).json({ message: response.data.message, locations: locations });
            }
            return res.status(response.status).json({ message: response.data.message })
            
        })
        .catch(function (error) 
        {
            return res.status(error.response.status).json({ message: error.response.data.message });
        });

    } catch(err) {
        return res.status(err.response.status).json({ message: err.message });
    }
};

const getLocationById = async (req, res) => 
{
    try
    {
        var id = req.params.id;
        let config = 
        {
            headers: {  Authorization: req.get('Authorization') }
        }
        axios.get(`${controller.journeyHost}/location/${id}`, config)
        .then(function (response) 
        {
            if (response.status == 200) {
                var location = {
                    id: response.data.location.id,
                    name: response.data.location.name,
                    description: response.data.location.description,
                    shortDescription: response.data.location.shortDescription,
                    categories: response.data.location.categories,
                    latitude: response.data.location.latitude,
                    longitude: response.data.location.longitude,
                    images: response.data.location.images,
                    tags: response.data.location.tags,
                    r1: response.data.location.r1,
                    r2: response.data.location.r2
                }
                return res.status(httpStatus.OK).json({ message: response.data.message, location: location });
            }
            return res.status(response.status).json({ message: response.data.message })
            
        })
        .catch(function (error) 
        {
            res.status(error.response.status).json({ message: error.response.data.message });
        });

    } catch(err) {
        return res.status(err.response.status).json({ message: err.message });
    }
};

const create = async (req, res, next) => 
{
    try
    {
        return upload(req, res, async(err) => {
            if (err) {
                res.status(err.status || 500).json({
                    message: err.message
                });
            }
            let imageFiles = req.files;
            let form = new FormData();
            if (imageFiles.length != 0) 
            {
                for(let i = 0; i < imageFiles.length; i++) {
                    form.append('images', fs.createReadStream(imageFiles[i].path), imageFiles[i].filename)
                }
            }
            form.append('name', req.body.name);
            form.append('description', req.body.description);
            form.append('tags', req.body.tags);
            form.append('latitude', req.body.latitude);
            form.append('longitude', req.body.longitude)
            form.append('shortDescription', req.body.shortDescription)
            form.append('r1', req.body.r1)
            form.append('r2', req.body.r2)
            axios.post(`${controller.journeyHost}/admin/location`, form, {
                headers: {
                    'Authorization': req.get('Authorization'),
                    'Content-Type': `multipart/form-data; boundary=${form._boundary}`
                }
            })
            .then(function (response) 
            {
                if (response.status === 201) { 
                    return res.status(httpStatus.OK).json({ message: response.data.message, location: response.data.location });
                }
                return res.status(response.status).json({ message: response.data.message })

            })
            .catch(function (error) 
            {
                res.status(error.response.status).json({ message: error.response.data.message });
            })
        })  
    } catch(err) {
        return res.status(err.response.status).json({ message: err.message });
    }
};

const deleteLocation = async (req, res) => 
{
    try
    {
        let config = 
        {
            headers: {  Authorization: req.get('Authorization') }
        }

        axios.delete(`${controller.journeyHost}/admin/location/${req.params.id}`, config)
        .then(function (response) 
        {
            if (response.status === 200) {
                return res.status(response.status).json({ message: response.data.message })
            } else {
                return response.data.message
            }  
        })
        .catch(function (error) 
        {
            return res.status(error.response.status).json({ message: error.response.data.message })
        });
    } catch(err) {
        return res.status(err.response.status).json({ message: err.message })
    }
};

const updateLocation = async (req, res) => 
{
    try
    {
        return upload(req, res, async(err) => {
            if (err) {
                res.status(err.status || 500).json({
                    message: err.message
                });
            }
            let imageFiles = req.files;
            let form = new FormData();
            if (imageFiles.length != 0) 
            {
                for(let i = 0; i < imageFiles.length; i++) {
                    form.append('images', fs.createReadStream(imageFiles[i].path), imageFiles[i].filename)
                }
            }
            for(var key in req.body)
            {
                form.append(key, req.body[key]);
            }
            axios.put(`${controller.journeyHost}/admin/location/${req.params.id}`, form, {
                headers: {
                    'Authorization': req.get('Authorization'),
                    'Content-Type': `multipart/form-data; boundary=${form._boundary}`
                }
            })
            .then(function (response) 
            {
                if (response.status === 200) { 
                    return res.status(httpStatus.OK).json({ message: response.data.message, location: response.data.location });
                }
                return res.status(response.status).json({ message: response.data.message })

            })
            .catch(function (error) 
            {
                res.status(error.response.status).json({ message: error.response.data.message });
            })
        })
    } catch(err) {
        return res.status(err.response.status).json({ message: err.message })
    }
}

module.exports = 
{
    getLocations,
    getLocationById,
    create,
    deleteLocation,
    updateLocation
}