const httpStatus = require('http-status');
const axios = require('axios');
const journeyHost = 'http://35.240.184.36:3000';
const adminToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOnsiaWQiOiJmZjBjYzE2ZS1iMjc2LTQ3M2UtOWY3OS03NzJhODkzNTU1OTkiLCJzb2NpYWxJZCI6bnVsbCwic29jaWFsVG9rZW4iOm51bGwsInNvY2lhbFRva2VuU2VjcmV0IjpudWxsLCJuYW1lIjoiYWRtaW4iLCJzb2NpYWxVc2VybmFtZSI6bnVsbCwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJwYXNzd29yZCI6IiQyYiQxMCRkQTd6d2Z6bXRTRkdJYXVZcnRLSmUuMFVTbldnN1p3cWZIYmlveEFVaHBKRVBDUnVmTWJZYSIsInNvY2lhbEFnZVJhbmdlIjpudWxsLCJzb2NpYWxJbWFnZVVybCI6bnVsbCwiaXNBZG1pbiI6dHJ1ZSwiaXNEZWxldGVkIjpmYWxzZSwiY3JlYXRlZEF0IjoiMjAxOS0wMi0xNVQwNDo1NjoxMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wMi0xNVQwNDo1NjoxMi4wMDBaIn0sImlhdCI6MTU1ODk3OTI3MTg3NCwiZXhwIjoxNTYwMjc1MjcxfQ.DkPJAyhP4gfSfP7_AuUas0oaPf3vUrU37TfDjGvAwBs';
// const adminToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOnsiaWQiOiJmZjBjYzE2ZS1iMjc2LTQ3M2UtOWY3OS03NzJhODkzNTU1OTkiLCJzb2NpYWxJZCI6bnVsbCwic29jaWFsVG9rZW4iOm51bGwsInNvY2lhbFRva2VuU2VjcmV0IjpudWxsLCJuYW1lIjoiYWRtaW4iLCJzb2NpYWxVc2VybmFtZSI6bnVsbCwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJwYXNzd29yZCI6IiQyYiQxMCRkQTd6d2Z6bXRTRkdJYXVZcnRLSmUuMFVTbldnN1p3cWZIYmlveEFVaHBKRVBDUnVmTWJZYSIsInNvY2lhbEFnZVJhbmdlIjpudWxsLCJzb2NpYWxJbWFnZVVybCI6bnVsbCwiaXNBZG1pbiI6dHJ1ZSwiaXNEZWxldGVkIjpmYWxzZSwiY3JlYXRlZEF0IjoiMjAxOS0wMi0xNVQwNDo1NjoxMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wMi0xNVQwNDo1NjoxMi4wMDBaIn0sImlhdCI6MTU1NTUwODU5MTI4MiwiZXhwIjoxNTU2ODA0NTkxfQ.3ClmszngF_rM087AC794sERkT91NY9eRUDWsFXEfSmM'
let FormData = require('form-data');
let fs = require('fs');
let multer = require('multer');
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/images/books')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + '_' + file.originalname)
    }
});
let upload = multer({
    storage: storage
}).single('coverImage');

const getBooks = async (req, res) => 
{
    try
    {
        let userConfig = {  headers: {  Authorization: req.get('Authorization') }   } 
        axios.get(`${journeyHost}/trip`, userConfig)
        .then(function (tripResponse) 
        {
            if (tripResponse.status === 200) 
            {
                let planIds = []
                tripResponse.data.trips.forEach(trip => {
                    planIds.push(trip.plan.id)
                })
                axios.get(`${journeyHost}/plan`, {
                    headers: {  Authorization: adminToken },
                    params: { pageSize: 70 }
                })
                .then(async function (response) 
                {
                    if (response.status === 200) {
                        let books = [];
                        response.data.plans.forEach(plan => {
                            let isDownload = false
                            if (planIds.indexOf(plan.id) > -1)
                            {
                                isDownload = true;
                            }
                            let book = {
                                id: plan.id,
                                name: plan.name,
                                description: plan.description,
                                shortDescription: plan.shortDescription,
                                imagePath: plan.imagePath,
                                latitude: plan.latitude,
                                longitude: plan.longitude,
                                isDownloaded: isDownload,
                                tags: plan.tags,
                                area: plan.area,
                                isNew: plan.isNew,
                                locationCount: plan.planItems.length
                            }
                            books.push(book);
                        });
                        return res.status(httpStatus.OK).json({ message: response.data.message, books: books });
                    }
                    return res.status(response.status).json({ message: response.data.message })
                    
                })
                .catch(function (error) 
                {
                    res.status(error.response.status).json({ message: error.response.data.message });
                });
            }
            else {
                return res.status(tripResponse.status).json({ message: tripResponse.data.message })
            }
        })
        .catch(function (errorResponse) 
        {
            res.status(errorResponse.response.status).json({ message: errorResponse.response.data.message });
        });
    } catch(err) {
        return res.status(err.response.status).json({ message: err.message });
    }
};

//waiting for admin token
const getBookById = async (req, res) => 
{
    try
    {
        let id = req.params.id;
        let config = 
        {
            headers: {  Authorization: adminToken }
        }
        axios.get(`${journeyHost}/plan/${id}`, config)
        .then(async function (response) 
        {
            if (response.status === 200) {
                let locationIds = [];
                response.data.plan.planItems.forEach(item => {
                    let location = {
                        id: item.location.id,
                        order: item.order
                    }
                    locationIds.push(location)
                })
                let locationArr = []
                let previewImages = []
                for (var i = 0; i < locationIds.length; i++)
                {
                    let getLocation = await axios.get(`${journeyHost}/location/${locationIds[i].id}`, config);
                    let locationOrder = getLocation.data.location
                    locationOrder.order = locationIds[i].order
                    locationArr.push(locationOrder)
                    if (locationOrder.images && locationOrder.images.length && previewImages.length != 6) {
                        previewImages.push(locationOrder.images[0].path)
                    }
                }
                let book = {
                    id: response.data.plan.id,
                    name: response.data.plan.name,
                    description: response.data.plan.description,
                    shortDescription: response.data.plan.shortDescription,
                    categories: response.data.plan.categories,
                    imagePath: response.data.plan.imagePath,
                    latitude: response.data.plan.latitude,
                    longitude: response.data.plan.longitude,
                    tags: response.data.plan.tags,
                    area: response.data.plan.area,
                    previewImages: previewImages,
                    locations: locationArr
                }
                return res.status(httpStatus.OK).json({ message: response.data.message, book: book });
            }
            return res.status(response.status).json({ message: response.data.message })
            
        })
        .catch(function (error) 
        {
            res.status(error.response.status).json({ message: error.response.data.message });
        });

    } catch(err) {
        return res.status(err.response.status).json({ message: err.message });
    }
};

const create = async (req, res, next) => 
{
    try
    {
        return upload(req, res, async(err) => {
            if (err) {
                res.status(err.status || 500).json({
                    message: err.message
                });
            }
            let imageFile = req.file;
            let form = new FormData();
            if (imageFile) 
            {
                form.append('coverImage', fs.createReadStream(imageFile.path), imageFile.filename)
            }
            form.append('name', req.body.name);
            form.append('description', req.body.description);
            form.append('shortDescription', req.body.shortDescription);
            form.append('tags', req.body.tags);
            form.append('latitude', req.body.latitude);
            form.append('longitude', req.body.longitude);
            form.append('isNew', req.body.isNew);
            form.append('area', req.body.area);
            if(req.body.locations) {
                for(let i = 0; i < req.body.locations.length; i++) {
                    form.append(`locations[${i}]`, req.body.locations[i])
                }
            }
            axios.post(`${journeyHost}/plan/`, form, {
                headers: {
                    'Authorization': req.get('Authorization'),
                    'Content-Type': `multipart/form-data; boundary=${form._boundary}`
                }
            })
            .then(function (response) 
            {
                if (response.status === 201) {
                    return res.status(httpStatus.CREATED).json({ message: response.data.message, book: response.data.plan });
                }
                return res.status(response.status).json({ message: response.data.message })

            })
            .catch(function (error) 
            {
                res.status(error.response.status).json({ message: error.response.data.message });
            })
        })
    } catch(err) {
        return res.status(err.response.status).json({ message: err.message });
    }
};

const download = async (req, res) => 
{
    try
    {
        let config = 
        {
            headers: {  Authorization: req.get('Authorization') }
        }
        var data = 
        { 
            'name': req.body.name,
            'description': req.body.description,
            'planId': req.body.id 
        }
        axios.post(`${journeyHost}/trip/`, data, config)
        .then(function (response) 
        {
            if (response.status === 201) {
                return res.status(response.status).json({ message: response.data.message, trip: response.data.trip })
            } else {
                return response.data.message
            }  
        })
        .catch(function (error) 
        {
            return res.status(error.response.status).json({ message: error.response.data.message });
        });
    } catch(err) {
        return res.status(err.response.status).json({ message: err.message });
    }
};

const deleteBook = async (req, res) => 
{
    try
    {
        let config = 
        {
            headers: {  Authorization: req.get('Authorization') }
        }
        axios.delete(`${journeyHost}/plan/${req.params.id}`, config)
        .then(function (response) 
        {
            if (response.status === 200) {
                return res.status(response.status).json({ message: response.data.message, message: response.data.message })
            } else {
                return response.data.message
            }  
        })
        .catch(function (error) 
        {
            return res.status(error.response.status).json({ message: error.response.data.message })
        });
    } catch(err) {
        return res.status(err.response.status).json({ message: err.message })
    }
};

const updateBook = async (req, res) => 
{
    try
    {
        return upload(req, res, async(err) => {
            if (err) {
                res.status(err.status || 500).json({
                    message: err.message
                });
            }
            let imageFile = req.file;
            let form = new FormData();
            if (imageFile) 
            {
                form.append('coverImage', fs.createReadStream(imageFile.path), imageFile.filename)
            }
            for(var key in req.body)
            {
                form.append(key, req.body[key]);
            }
            axios.put(`${journeyHost}/plan/${req.params.id}`, form, { 
                headers: {  
                    Authorization: adminToken,
                    'Content-Type': `multipart/form-data; boundary=${form._boundary}` } })
            .then(function (response) 
            {
                if (response.status === 200) {
                    return res.status(response.status).json({ message: response.data.message, book: response.data.plan })
                } else {
                    return res.status(response.status).json({ message: response.data.message })
                }  
            })
            .catch(function (error) 
            {
                return res.status(error.response.status).json({ message: error.response.data.message })
            });
    })
    } catch(err) {
        return res.status(err.response.status).json({ message: err.message })
    }
}

const exploreBook = async (req, res) => 
{
    try
    {
        //let config = {  headers: {  Authorization: adminToken, 'Content-type' : req.get('Content-Type') } }
        let data = req.body
        axios.all([axios.put(`${journeyHost}/plan/explore?byProximity=true`, data, {  
                        headers: {  Authorization: adminToken, 'Content-type': req.get('Content-Type') } 
                    }),
                   axios.put(`${journeyHost}/plan/explore`, { params : { byNewArrival: true } }, {
                        headers: {  Authorization: adminToken },
                        params: { byNewArrival: true } 
                    }),
                    axios.put(`${journeyHost}/plan/explore`, { params: { byPopularity: true }} , {
                        headers: {  Authorization: adminToken },
                        params: { byPopularity: true }
                    }),
                    axios.get(`${journeyHost}/trip` , {
                        headers: { Authorization: req.get('Authorization') }
                    })])
        .then(axios.spread((byProximityRes, byNewArrivalRes, byPopularityRes, trips) => {  
            if (byProximityRes.status === 201 && byNewArrivalRes.status === 201 && byPopularityRes.status === 201 && trips.status === 200) {
                let planIds = []
                trips.data.trips.forEach(trip => {
                    planIds.push(trip.plan.id)
                })
                let  byProximity = [], byNewArrival = [], byPopularity = [] 
                byProximityRes.data.plans.forEach(plan => {
                    let isDownloaded = false
                    if (planIds.indexOf(plan.id) > -1)
                    {
                        isDownloaded = true;
                    }
                    let book = {
                        id: plan.id,
                        name: plan.name,
                        description: plan.description,
                        shortDescription: plan.shortDescription,
                        imagePath: plan.imagePath,
                        tags: plan.tags,
                        area: plan.area,
                        isNew: plan.isNew,
                        latitude: plan.latitude,
                        longitude: plan.longitude,
                        createdAt: plan.createdAt,
                        updatedAt: plan.updatedAt,
                        isDeleted: plan.isDeleted,
                        distance: plan.distance,
                        isDownloaded: isDownloaded,
                    }
                    byProximity.push(book)
                })
                byNewArrivalRes.data.plans.forEach(plan => {
                    let isDownloaded = false
                    if (planIds.indexOf(plan.id) > -1)
                    {
                        isDownloaded = true;
                    }
                    let book = {
                        id: plan.id,
                        name: plan.name,
                        description: plan.description,
                        shortDescription: plan.shortDescription,
                        imagePath: plan.imagePath,
                        tags: plan.tags,
                        area: plan.area,
                        isNew: plan.isNew,
                        latitude: plan.latitude,
                        longitude: plan.longitude,
                        createdAt: plan.createdAt,
                        updatedAt: plan.updatedAt,
                        isDeleted: plan.isDeleted,
                        isDownloaded: isDownloaded
                    }
                    byNewArrival.push(book)
                })
                byPopularityRes.data.plans.forEach(plan => {
                    let isDownloaded = false
                    if (planIds.indexOf(plan.id) > -1)
                    {
                        isDownloaded = true;
                    }
                    let book = {
                        id: plan.id,
                        name: plan.name,
                        description: plan.description,
                        shortDescription: plan.shortDescription,
                        imagePath: plan.imagePath,
                        tags: plan.tags,
                        area: plan.area,
                        isNew: plan.isNew,
                        latitude: plan.latitude,
                        longitude: plan.longitude,
                        createdAt: plan.createdAt,
                        updatedAt: plan.updatedAt,
                        isDeleted: plan.isDeleted,
                        tripCount: plan.tripCount,
                        isDownloaded: isDownloaded
                    }
                    byPopularity.push(book)
                })

                return res.status(httpStatus.OK).json({ message: byProximityRes.data.message, byProximity: byProximity, 
                                                                                           byNewArrival: byNewArrival, 
                                                                                           byPopularity: byPopularity })
            }
            return res.status(httpStatus.BAD_REQUEST).json({ message: "Bad request"})
        }))
        .catch(function (error) 
        {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({ message: error.message })
        })
    } catch(err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({ message: err.message });
    }
}

const addLocation = async (req, res) => 
{
    try
    {
        let data = req.body
        let config = 
        {
            headers: { Authorization: req.get('Authorization'), 'Content-type' : req.get('Content-Type') }
        }
        axios.put(`${journeyHost}/plan/${req.params.id}/tripLocation`, data, config)
        .then(function (response) 
        {
            if (response.status == 200) {
                return res.status(httpStatus.OK).json({ message: response.data.message })
            }
            return res.status(response.status).json({ message: response.data.message })
            
        })
        .catch(function (error) 
        {
            return res.status(error.response.status).json({ message: error.response.data.message });
        })

    } catch(err) {
        return res.status(err.response.status).json({ message: err.message })
    }
}

module.exports = 
{
    getBooks,
    getBookById,
    journeyHost,
    create,
    download,
    adminToken,
    deleteBook,
    updateBook,
    exploreBook,
    addLocation
}