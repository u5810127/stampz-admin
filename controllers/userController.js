const httpStatus = require('http-status')
const axios = require('axios')
const controller = require('./bookController')

const login = async (req, res) => 
{
    try
    {
        let data = req.body
        let config = 
        {
            headers: { 'Content-Type': 'application/json' }
        }
        axios.post(`${controller.journeyHost}/auth/create`, data, config)
        .then(function (response) 
        {
            if (response.status == 201) {
                return res.status(httpStatus.OK).json({ message: response.data.message, user: response.data.user })
            }
            return res.status(response.status).json({ message: response.data.message })
            
        })
        .catch(function (error) 
        {
            return res.status(error.response.status).json({ message: error.response.data.message });
        })

    } catch(err) {
        return res.status(err.response.status).json({ message: err.message })
    }
}

const logout = async (req, res) => 
{
    try
    {
        var data = {
            'clientId': req.body.clientId,
            'clientSecret': req.body.clientSecret,
            'name': req.body.name,
        }
        let config = 
        {
            headers: { Authorization: controller.adminToken, 'Content-Type': 'application/json' }
        }
        axios.post(`${controller.journeyHost}/auth/logout`, data, config)
        .then(function (response) 
        {
            if (response.status == 200) {
                return res.status(httpStatus.OK).json({ message: response.data.message, user: response.data.user })
            }
            return res.status(response.status).json({ message: response.data.message })
            
        })
        .catch(function (error) 
        {
            return res.status(error.response.status).json({ message: error.response.data.message });
        })

    } catch(err) {
        return res.status(err.response.status).json({ message: err.message })
    }
}

const getNewToken = async (req, res) => 
{
    try
    {
        var data = { 'refreshToken': req.body.refreshToken }
        let config = 
        {
            headers: { 'Content-Type': 'application/json' }
        }
        axios.post(`${controller.journeyHost}/auth/token/new`, data, config)
        .then(function (response) 
        {
            if (response.status == 200) {
                return res.status(httpStatus.OK).json({ message: response.data.message, user: response.data.user })
            }
            return res.status(response.status).json({ message: response.data.message })
            
        })
        .catch(function (error) 
        {
            return res.status(error.response.status).json({ message: error.response.data.message });
        })

    } catch(err) {
        return res.status(err.response.status).json({ message: err.message })
    }
}

const getUserProfile = async (req, res) => 
{
    try
    {
        let config = 
        {
            headers: {  Authorization: req.get('Authorization') }
        }
        axios.get(`${controller.journeyHost}/user`, config)
        .then(function (response) 
        {
            if (response.status == 200) {
                let user = {
                    id: response.data.user.id,
                    email: response.data.user.email,
                    name: response.data.user.name,
                    socialUserName: response.data.user.socialUserName,
                    socialGender: response.data.user.socialGender,
                    socialImageUrl: response.data.user.SocialImageUrl
                }
                return res.status(httpStatus.OK).json({ message: response.data.message, user: user });
            }
            return res.status(response.status).json({ message: response.data.message })
            
        })
        .catch(function (error) 
        {
            res.status(error.response.status).json({ message: error.response.data.message });
        });

    } catch(err) {
        return res.status(err.response.status).json({ message: err.message });
    }
};

const getUserStat = async (req, res) => 
{
    try
    {
        let config = 
        {
            headers: {  Authorization: req.get('Authorization') }
        }
        axios.get(`${controller.journeyHost}/trip`, config)
        .then(function (response) 
        {
            let inProgress = 0
            let completed = 0
            let stamps = 0
            if (response.status == 200) {
                response.data.trips.forEach(item => {
                    if(item.status.id === 'e98abf1a-b585-463f-9398-d88e252d080d' || item.status.id === '3b6a7294-ffaf-46de-a37d-ccf565ba68d5') {
                        inProgress = inProgress + 1
                    } else if (item.status.id === '7d75fd87-3197-4e9c-911d-50a14e8e02af') {
                        completed = completed + 1
                    }
                    stamps = stamps + item.checkIns.length
                });
                return res.status(httpStatus.OK).json({ message: response.data.message, user: { inProgress: inProgress, completed: completed, stampCount: stamps} });
            }
            return res.status(response.status).json({ message: response.data.message }) 
        })
        .catch(function (error) 
        {
            res.status(error.response.status).json({ message: error.response.data.message });
        });

    } catch(err) {
        return res.status(err.response.status).json({ message: err.message });
    }
};

module.exports = 
{
    login,
    logout,
    getNewToken,
    getUserProfile,
    getUserStat
}